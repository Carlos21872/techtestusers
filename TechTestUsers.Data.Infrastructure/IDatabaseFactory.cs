﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestUsers.Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        TechTestUsersContext Get();
    }
}
