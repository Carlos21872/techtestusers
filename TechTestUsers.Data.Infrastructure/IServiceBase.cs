﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TechTestUsers.Data.Infrastructure
{
    public interface IServiceBase<T> where T : class
    {

        Task<T> Get(int id);
        Task<T> Get(Expression<Func<T, bool>> where);
        Task<bool> Any(Expression<Func<T, bool>> where);
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where);
        Task<IEnumerable<T>> GetMany(Expression<Func<T, bool>> where, string order, bool descending, int take);

        Task<int> Update(T entity);
        Task<int> Save();
        Task<int> Create(T entity);
        Task<int> Delete(int id);
        Task<int> Count(Expression<Func<T, bool>> where);
        Task<int> CountAll();
    }
}
