﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TechTestUsers.Core.Enums;

namespace TechTestUsers.Data.Entities.Models
{
    public class ApplicationUser : IdentityUser
    {
        //public string Usuario { get; set; } Tomar UserName de Identity
        public Sexo Sexo { get; set; }
        public Status Estatus { get; set; }
        public DateTime FechaCreacion { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
