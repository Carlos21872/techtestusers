﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestUsers.Data.Entities.Models;

namespace TechTestUsers.Data
{
    public class TechTestUsersContext : IdentityDbContext<ApplicationUser>
    {
        public TechTestUsersContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static TechTestUsersContext Create()
        {
            return new TechTestUsersContext();
        }

        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            if ((entityEntry != null) && (entityEntry.State == EntityState.Added))
            {
                ApplicationUser user = entityEntry.Entity as ApplicationUser;
                if ((user != null) && this.Users.Any<ApplicationUser>(u => string.Equals(u.UserName, user.UserName)))
                {
                    return new DbEntityValidationResult(entityEntry, new List<DbValidationError>()) { ValidationErrors = { new DbValidationError("User", "El nombre de usuario ya existe en el sistema") } };
                }
                IdentityRole role = entityEntry.Entity as IdentityRole;
                if ((role != null) && this.Roles.Any<IdentityRole>(r => string.Equals(r.Name, role.Name)))
                {
                    return new DbEntityValidationResult(entityEntry, new List<DbValidationError>()) { ValidationErrors = { new DbValidationError("Role", "El nombre del rol ya existe en el sistema") } };
                }
            }
            return base.ValidateEntity(entityEntry, items);
        }
    }
}
