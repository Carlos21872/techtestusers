namespace TechTestUsers.Data.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TechTestUsers.Data.Entities.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TechTestUsers.Data.TechTestUsersContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(TechTestUsers.Data.TechTestUsersContext context)
        {
            //  This method will be called after migrating to the latest version.
            var user = new Entities.Models.ApplicationUser
            {
                Email = "admin@test.com",
                UserName = "Administrador",
                PhoneNumber = "+111111111111",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
                FechaCreacion = DateTime.Now
            };


            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var hashed = userManager.PasswordHasher.HashPassword("Secret123@");
                user.PasswordHash = hashed;
            }

            context.Users.AddOrUpdate(
              p => p.UserName,
              user
            );


            context.SaveChanges();
            //
        }
    }
}
