﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestUsers.Core.Utils
{
    public static class RegexValidations
    {
        public const string REGEXPASSWORD = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W).*$";
    }
}
