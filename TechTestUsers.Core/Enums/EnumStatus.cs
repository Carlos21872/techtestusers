﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestUsers.Core.Enums
{
    public enum Status
    {
        Activo,
        Inactivo
    }
}
