﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestUsers.Core.Enums
{
    public enum Sexo
    {
        Masculino,
        Femenino,
        Otro
    }
}
