﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TechTestUsers.Web.Startup))]
namespace TechTestUsers.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
