﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TechTestUsers.Web.Models
{
    public class LoginViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "El campo {0} debe tener al menos {2} caracteres.", MinimumLength = 7)]
        public string Usuario { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
    }

}
