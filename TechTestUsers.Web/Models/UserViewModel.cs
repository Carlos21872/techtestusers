﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TechTestUsers.Core.Enums;
using TechTestUsers.Core.Utils;

namespace TechTestUsers.Web.Models
{
    public class UserViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "El campo usuario es obligatorio.")]
        [StringLength(100, ErrorMessage = "El campo {0} debe tener al menos {2} caracteres.", MinimumLength = 7)]
        public string Usuario { get; set; }

        [Required(ErrorMessage = "El campo contraseña es obligatorio.")]
        [StringLength(100, ErrorMessage = "La contraseña debe tener al menos {2} caracteres.", MinimumLength = 10)]
        [RegularExpression(RegexValidations.REGEXPASSWORD, ErrorMessage = "Contraseña inválida: Debe contener al menos 1 Mayúscula, 1 Minúscula, 1 Dígito y 1 Caracter especial")] 
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La confirmación de contraseña no coincide con la contraseña escrita")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "El campo Email es obligatorio.")]
        [DataType(DataType.EmailAddress, ErrorMessage = "El campo Email, debe ser un correo electronico válido.")]
        public string Email { get; set; }

        public Sexo Sexo { get; set; }

    }
}