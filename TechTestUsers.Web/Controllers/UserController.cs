﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TechTestUsers.Core.Utils;
using TechTestUsers.Data.Entities.Models;
using TechTestUsers.Data.Infrastructure;
using TechTestUsers.Services.Interfaces;
using TechTestUsers.Services.Services;
using TechTestUsers.Web.Models;

namespace TechTestUsers.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private IUserService _userService;
        private ApplicationUserManager _userManager;

        public UserController()
        {
            _userService = new UserService(new DatabaseFactory());
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: User
        public async Task<ActionResult> Index()
        {
            var users = await _userService.GetAll();

            return View(users);
        }

        public ActionResult Create()
        {
            return View(new UserViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ApplicationUser user = new ApplicationUser();

                    user.UserName = model.Usuario;
                    user.Email = model.Email;
                    user.EmailConfirmed = true;
                    user.FechaCreacion = DateTime.Now;
                    user.Estatus = Core.Enums.Status.Activo;
                    user.Sexo = model.Sexo;
                    user.SecurityStamp = Guid.NewGuid().ToString("D");
                    var hashed = UserManager.PasswordHasher.HashPassword(model.Password);
                    user.PasswordHash = hashed;

                    await _userService.Create(user);

                    return RedirectToAction("Index");

                }
                else
                {
                    ModelState.AddModelError("", "Error al guardar los datos del usuario. Verifique los datos.");
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }

                ModelState.AddModelError("", "Error al guardar los datos del usuario. Verifique los datos.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in Create:"+ex.Message);
                ModelState.AddModelError("", "Error al guardar los datos del usuario. Intente mas tarde.");
            }

            return View(model);
        }

        public async Task<ActionResult> Edit(string Id)
        {
            var user = await _userService.Get(i => i.Id == Id);

            if(user == null)
            {
                //Mensaje
                return RedirectToAction("Index");
            }

            var model = new UserViewModel()
            {
                Email = user.Email,
                Id = user.Id,
                Sexo = user.Sexo,
                Usuario = user.UserName,
                Password = Utils.base64Decode(user.PasswordHash),
                ConfirmPassword = Utils.base64Decode(user.PasswordHash)
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UserViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userService.Get(i => i.Id == model.Id);

                    if(user == null)
                    {
                        ModelState.AddModelError("", "El usuario no existe.");
                        return View(model);
                    }

                    user.UserName = model.Usuario;
                    user.Email = model.Email;
                    user.EmailConfirmed = true;
                    user.Estatus = user.Estatus;
                    user.Sexo = model.Sexo;
                    var hashed = UserManager.PasswordHasher.HashPassword(model.Password);
                    user.PasswordHash = hashed;
                    
                    await _userService.Update(user);

                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Error al actualizar los datos del usuario. Verifique los datos.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in Create:" + ex.Message);
                ModelState.AddModelError("", "Error al actualizar los datos del usuario. Intente mas tarde.");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string Id)
        {
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    var todelete = await _userService.Get(i=>i.Id == Id);
                    todelete.Estatus = Core.Enums.Status.Inactivo;
                    await _userService.Update(todelete);

                    return Json(new { success = true, message = "" }, JsonRequestBehavior.AllowGet);
                }

                return Content("Error al intentar eliminar, intente mas tarde.");
            }
            catch (Exception ex)
            {
                return Content("Error: " + ex.Message);
            }
        }

    }
}