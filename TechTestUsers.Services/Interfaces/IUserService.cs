﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestUsers.Data.Entities.Models;
using TechTestUsers.Data.Infrastructure;

namespace TechTestUsers.Services.Interfaces
{
    public interface IUserService : IServiceBase<ApplicationUser>
    {
    }
}
