﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestUsers.Data.Entities.Models;
using TechTestUsers.Data.Infrastructure;
using TechTestUsers.Services.Interfaces;

namespace TechTestUsers.Services.Services
{
    public class UserService : ServiceBase<ApplicationUser>, IUserService
    {
        public UserService(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
}
